// ......................................................
// .......................Firebase Init..................
// ......................................................
const firebaseConfig = {
    apiKey: "AIzaSyDFUKLyK1sIWs9DNtqea5mLnN5dBOTGrS4",
    authDomain: "smarthome-database.firebaseapp.com",
    databaseURL: "https://smarthome-database.firebaseio.com",
    projectId: "smarthome-database",
    storageBucket: "smarthome-database.appspot.com",
    messagingSenderId: "589726003713",
    appId: "1:589726003713:web:10abb75ebbdde615"
}

firebase.initializeApp(firebaseConfig);
const database = firebase.database();
console.log(database);


// ......................................................
// ..................RTCMultiConnection Code.............
// ......................................................

var connection = new RTCMultiConnection();
connection.socketURL = 'http://localhost:3000/';
connection.socketMessageEvent = 'video-broadcast-demo';

connection.session = {
    video: true,
    audio: false,
    oneway: true
};

connection.sdpConstraints.mandatory = {
    OfferToReceiveVideo: false,
    OfferToReceiveAudio: false,
};

connection.videosContainer = document.getElementById('videos-container');
connection.onstream = function(event) {
    var existing = document.getElementById(event.streamid);
    if(existing && existing.parentNode) {
      existing.parentNode.removeChild(existing);
    }

    event.mediaElement.removeAttribute('src');
    event.mediaElement.removeAttribute('srcObject');
    event.mediaElement.muted = true;
    event.mediaElement.volume = 0;

    var video = document.createElement('video');

    try {
        video.setAttributeNode(document.createAttribute('autoplay'));
        video.setAttributeNode(document.createAttribute('playsinline'));
    } catch (e) {
        video.setAttribute('autoplay', true);
        video.setAttribute('playsinline', true);
    }

    video.volume = 0;
    try {
        video.setAttributeNode(document.createAttribute('muted'));
    } catch (e) {
        video.setAttribute('muted', true);
    }

    video.srcObject = event.stream;

    connection.videosContainer.appendChild(video);

    setTimeout(function() {
        video.play();
    }, 5000);

    mediaElement.id = event.streamid;
};

connection.onstreamended = function(event) {
    var mediaElement = document.getElementById(event.streamid);
    if (mediaElement) {
        mediaElement.parentNode.removeChild(mediaElement);

        if(event.userid === connection.sessionid && !connection.isInitiator) {
          alert('Broadcast is ended. We will reload this page to clear the cache.');
          location.reload();
        }
    }
};

connection.onMediaError = function(e) {
    if (e.message === 'Concurrent mic process limit.') {
        if (DetectRTC.audioInputDevices.length <= 1) {
            alert('Please select external microphone. Check github issue number 483.');
            return;
        }

        var secondaryMic = DetectRTC.audioInputDevices[1].deviceId;
        connection.mediaConstraints.audio = {
            deviceId: secondaryMic
        };

        connection.join(connection.sessionid);
    }
};

// ..................................
// ALL below scripts are redundant!!!
// ..................................

// ......................................................
// ......................Handling Room-ID................
// ......................................................
var roomid = '';
if (localStorage.getItem(connection.socketMessageEvent)) {
    roomid = localStorage.getItem(connection.socketMessageEvent);
} else {
    roomid = connection.token();
}

database.ref('livestreamId').set(roomid);
database.ref('livestreamId').on('value', (snapshot) => {
    console.log(snapshot.val());
    roomid = snapshot.val();
    localStorage.setItem(connection.socketMessageEvent, roomid);

    if (roomid && roomid.length) {
        // auto-join-room
        (function reCheckRoomPresence() {
            connection.checkPresence(roomid, function(isRoomExist) {
                if (!isRoomExist) {
                    connection.open(roomid);
                    return;
                }
    
                setTimeout(reCheckRoomPresence, 5000);
            });
        })();
    }
});

// detect 2G
if(navigator.connection &&
   navigator.connection.type === 'cellular' &&
   navigator.connection.downlinkMax <= 0.115) {
  alert('2G is not supported. Please use a better internet service.');
}